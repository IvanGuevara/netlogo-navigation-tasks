import matplotlib.pyplot as plt
import numpy as np
import csv

line_count_RS = 0
line_count_OS = 0
line_count_NS = 0
score_avg_RS = 0
score_avg_OS = 0
score_avg_NS = 0

with open('RS_1-1.csv', newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     for row in datareader:
         if line_count_RS > 0:
             score_avg_RS = score_avg_RS + int(row[2])
         line_count_RS = line_count_RS + 1
     score_avg_RS = score_avg_RS/(line_count_RS - 1)
with open('OS_1-1.csv', newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     for row in datareader:
         if line_count_OS > 0:
             score_avg_OS = score_avg_OS + int(row[2])
         line_count_OS = line_count_OS + 1
     score_avg_OS = score_avg_OS/(line_count_OS - 1)
with open('NS_1-1.csv', newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     for row in datareader:
         if line_count_NS > 0:
             score_avg_NS = score_avg_NS + int(row[2])
         line_count_NS = line_count_NS + 1
     score_avg_NS = score_avg_NS/(line_count_NS - 1)
plt.axhline(y=score_avg_RS, color='r', linestyle='-')
plt.axhline(y=score_avg_OS, color='g', linestyle='-')
plt.axhline(y=score_avg_NS, color='g', linestyle='-')
plt.annotate('RS', xy=(10,score_avg_RS+1))
plt.annotate('OS', xy=(10,score_avg_OS+1))
plt.annotate('NS', xy=(10,score_avg_NS+1))
plt.axis([0,100,0,100])
plt.ylabel('Hits average')
plt.xlabel('number of robots')
plt.show()
plt.savefig('score_average.eps', format='eps')         
