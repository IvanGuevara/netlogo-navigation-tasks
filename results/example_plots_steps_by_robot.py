import matplotlib.pyplot as plt
import numpy as np
import csv

line_count_RS = 0
line_count_OS = 0
line_count_NS = 0
steps_avg_RS = []
steps_avg_OS = []
steps_avg_NS = []

with open('RS_1-1.csv', newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     for row in datareader:
         if line_count_RS > 0:
             steps_avg_RS.append(int(row[3]))
         line_count_RS = line_count_RS + 1
     steps_avg_RS.sort()
with open('OS_1-1.csv', newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     for row in datareader:
         if line_count_OS > 0:
             steps_avg_OS.append(int(row[3]))
         line_count_OS = line_count_OS + 1
     steps_avg_OS.sort()
with open('NS_1-1.csv', newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     for row in datareader:
         if line_count_NS > 0:
             steps_avg_NS.append(int(row[3]))
         line_count_NS = line_count_NS + 1
     steps_avg_NS.sort()
t_RS = np.arange(0., float(len(steps_avg_RS)) , 1)
t_OS = np.arange(0., float(len(steps_avg_OS)) , 1)
t_NS = np.arange(0., float(len(steps_avg_NS)) , 1)
plt.axis([0, float(len(steps_avg_RS)),0,700])
plt.axis([0, float(len(steps_avg_OS)),0,700])
plt.axis([0, float(len(steps_avg_NS)),0,700])
plt.plot(t_RS, steps_avg_RS)
plt.plot(t_OS, steps_avg_OS)
plt.plot(t_NS, steps_avg_NS)
plt.annotate('RS', xy=(10,steps_avg_RS[10]+1))
plt.annotate('OS', xy=(10,steps_avg_OS[10]+1))
plt.annotate('NS', xy=(10,steps_avg_NS[10]+1))
plt.ylabel('Steps average')
plt.xlabel('number of robots')
plt.savefig('steps_average.png')         
