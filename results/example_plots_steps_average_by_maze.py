import matplotlib.pyplot as plt
import numpy as np
import csv

line_count_RS = 0
line_count_OS = 0
line_count_NS = 0
maze_avg_RS = []
maze_avg_OS = []
maze_avg_NS = []
files_RS = ['RS_1-1.csv', 'RS_1-2.csv', 'RS_1-3.csv', 'RS_1-4.csv', 'RS_1-5.csv']
files_OS = ['OS_1-1.csv', 'OS_1-2.csv', 'OS_1-3.csv', 'OS_1-4.csv', 'OS_1-5.csv']
files_NS = ['NS_1-1.csv', 'NS_1-2.csv', 'NS_1-3.csv', 'NS_1-4.csv', 'NS_1-5.csv']

for file in files_RS:
    with open(file, newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     steps_avg_RS = 0
     for row in datareader:
         if line_count_RS > 0:
             steps_avg_RS = steps_avg_RS + int(row[3])
         line_count_RS = line_count_RS + 1
     steps_avg_RS = steps_avg_RS/(line_count_RS - 1)
     maze_avg_RS.append(steps_avg_RS)
    line_count_RS = 0 

for file in files_OS:
    with open(file, newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     steps_avg_OS = 0
     for row in datareader:
         if line_count_OS > 0:
             steps_avg_OS = steps_avg_OS + int(row[3])
         line_count_OS = line_count_OS + 1
     steps_avg_OS = steps_avg_OS/(line_count_OS - 1)
     maze_avg_OS.append(steps_avg_OS)
    line_count_OS = 0 

for file in files_NS:
    with open(file, newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     steps_avg_NS = 0
     for row in datareader:
         if line_count_NS > 0:
             steps_avg_NS = steps_avg_NS + int(row[3])
         line_count_NS = line_count_NS + 1
     steps_avg_NS = steps_avg_NS/(line_count_NS - 1)
     maze_avg_NS.append(steps_avg_NS)
    line_count_NS = 0 
plt.axis([1,5,0,700])
plt.plot([1,2,3,4,5], maze_avg_RS, '.r-')
plt.plot([1,2,3,4,5], maze_avg_OS, '.g-')
plt.plot([1,2,3,4,5], maze_avg_NS, '.b-')
plt.annotate('RS', xy=(10,steps_avg_RS+1))
plt.annotate('OS', xy=(10,steps_avg_OS+1))
plt.annotate('NS', xy=(10,steps_avg_NS+1))
plt.ylabel('Steps average')
plt.xlabel('Maze number')
plt.xticks([1, 2, 3, 4, 5],
           ["Maze\n1-1", "Maze\n1-2", "Maze\n1-3", "Maze\n1-4", "Maze\n1-5"])    
plt.savefig('steps_average.png')
plt.show()
