import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import csv, sys

# Fixing random state for reproducibility
np.random.seed(19680801)

hit_min_NS = sys.maxsize
hit_max_NS = -sys.maxsize -1
line_count_NS = 0
hit_min_OS = sys.maxsize
hit_max_OS = -sys.maxsize -1
line_count_OS = 0
hit_min_RS = sys.maxsize
hit_max_RS = -sys.maxsize -1
line_count_RS = 0

with open('NS_1-1.csv', newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     for row in datareader:
         if line_count_NS > 0:
             if int(row[2]) < hit_min_NS:
                 hit_min_NS = int(row[2])
             elif int(row[2]) >= hit_max_NS:
                 hit_max_NS = int(row[2])
         line_count_NS = line_count_NS + 1
with open('OS_1-1.csv', newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     for row in datareader:
         if line_count_OS > 0:
             if int(row[2]) < hit_min_OS:
                 hit_min_OS = int(row[2])
             elif int(row[2]) >= hit_max_OS:
                 hit_max_OS = int(row[2])
         line_count_OS = line_count_OS + 1
with open('RS_1-1.csv', newline='') as csvfile:
     datareader = csv.reader(csvfile, delimiter=',')
     for row in datareader:
         if line_count_RS > 0:
             if int(row[2]) < hit_min_RS:
                 hit_min_RS = int(row[2])
             elif int(row[2]) >= hit_max_RS:
                 hit_max_RS = int(row[2])
         line_count_RS = line_count_RS + 1
     spread_NS = np.random.randint(hit_min_NS, hit_max_NS, size=50)
     center_NS = np.ones(10) * (hit_max_NS+hit_min_NS)/2
     flier_high_NS = np.random.rand(5) 
     flier_low_NS = np.random.rand(5)
     data_NS = np.concatenate((spread_NS, center_NS, flier_high_NS, flier_low_NS))

     spread_OS = np.random.randint(hit_min_OS, hit_max_OS, size=50)
     center_OS = np.ones(10) * (hit_max_OS+hit_min_OS)/2
     flier_high_OS = np.random.rand(5) 
     flier_low_OS = np.random.rand(5)
     data_OS = np.concatenate((spread_OS, center_OS, flier_high_OS, flier_low_OS))

     spread_RS = np.random.randint(hit_min_RS, hit_max_RS, size=50)
     center_RS = np.ones(10) * (hit_max_RS+hit_min_RS)/2
     flier_high_RS = np.random.rand(5) 
     flier_low_RS = np.random.rand(5)
     data_RS = np.concatenate((spread_RS, center_RS, flier_high_RS, flier_low_RS))
     top = 100
     bottom = 0
     names = ['NS', 'OS', 'RS']
     data = [data_NS, data_OS, data_RS]
     print(data)
     fig, ax7 = plt.subplots()
     ax7.set_ylim(bottom, top)
     ax7.set_xticklabels(names)
     bp = ax7.boxplot(data, showfliers=False, notch=True)
     colours = ['blue', 'green', 'red']
     for box, colour in zip(bp['boxes'],colours) :
         box.set(color=colour, linewidth=2)
     plt.savefig('myfig.eps', format='eps')         
    
